package com.android.markwhisperers.msqdeliveryapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartItemFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "user_key";
    // TODO: Customize parameters
    private String mKey;

    List<String> pricing = new ArrayList<>();
    MyCartItemRecyclerViewAdapter adapter;

    Button btnDeliveryComplete;

    ProgressDialog pDialog;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CartItemFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CartItemFragment newInstance(String mKey) {
        CartItemFragment fragment = new CartItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_COLUMN_COUNT, mKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mKey = getArguments().getString(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cartitem_list, container, false);

        adapter = new MyCartItemRecyclerViewAdapter(getActivity(), pricing);

        pDialog = new ProgressDialog(getActivity());
        pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pDialog.setMessage("Completing delivery. Please wait...");

        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);

        btnDeliveryComplete = view.findViewById(R.id.complete);


        FirebaseDatabase.getInstance().getReference().child("deliveries").child("pending").child(mKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    for (final DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        for (DataSnapshot _snapShot : snapshot.getChildren()) {
                            for (DataSnapshot snapSnap : _snapShot.getChildren()) {
                                System.out.println(snapSnap.getValue());
                                pricing.add(snapSnap.child("product").child("price").getValue().toString());
                            }
                        }

                        adapter.notifyDataSetChanged();

                        btnDeliveryComplete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pDialog.show();
                                Map<String, Object> childUpdates = new HashMap<>();
                                childUpdates.put("/deliveries/completed/" + snapshot.getKey(), dataSnapshot.getValue());
                                FirebaseDatabase.getInstance().getReference().updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            snapshot.getRef().removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        pDialog.dismiss();
                                                        adapter.notifyDataSetChanged();
                                                        Toast.makeText(getActivity(), "Delivery Complete", Toast.LENGTH_SHORT).show();
                                                        getFragmentManager().popBackStack();
                                                    } else {
                                                        Toast.makeText(getActivity(), "Could not complete. Please try again later", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            });
                                        } else {
                                            task.getException().printStackTrace();
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return view;
    }

}
